#!/bin/sh

# Reinitialiser
iptables -F
iptables -X

# politique par defaut
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT DROP

iptables -t nat -P DROP
iptables -t nat -P POSTROUTING DROP
iptables -t nat -P OUTPUT DROP

iptables -t mangle -P PREROUTING DROP
iptables -t mangle -P INPUT DROP
iptables -t mangle -P FORWARD DROP
iptables -t mangle -P OUTPUT DROP
iptables -t mangle -P POSTROUTING DROP
