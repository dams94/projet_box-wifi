<?php
require_once ('../jpgraph/src/jpgraph.php');
require_once ('../jpgraph/src/jpgraph_line.php');

$datay2=array();


$bdd1 = new PDO('mysql:host=127.0.0.1;dbname=res', 'root', '');
$i=4;
$req1=$bdd1->prepare("SELECT * FROM `debit` WHERE mode='down' ORDER BY id DESC LIMIT 0, 5 ");
$req1->execute();
foreach($req1 as $value2) {
	$datay2[$i]=$value2['vitesse'];

	$i--;
}
$bdd1=null;

$graph = new Graph(960,600);
//$graph->ClearTheme(); 
$graph->title->Set('Historique des 5 derniers tests de debit descendant');
$graph->title->SetFont(FF_FONT1,FS_BOLD, 10);
$graph->SetScale('intlin');
$graph->graph_theme = null;
$graph->SetBox(false);

$graph->yaxis->HideZeroLabel();
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

$graph->xaxis->SetTickLabels(array('1','2','3','4','5'));
$graph->ygrid->SetFill(false);

$p2 = new LinePlot($datay2);

$graph->add($p2);




$p2->SetColor('red@0.4');
$p2->SetLegend('Line 2');
$p2->mark->Show();
$p2->mark->SetType(MARK_UTRIANGLE,'',1.0);
$p2->mark->SetColor('red@0.4');
$p2->mark->SetFillColor('red@0.4');
$p2->value->SetMargin(14);
$p2->value->Show();
$p2->value->SetColor('red@0.4');
$p2->value->SetFormat('%01.2f');
$p2->SetCenter();

$graph->legend->SetMarkAbsSize(8);
$graph->legend->SetFrameWeight(1);
$graph->yaxis->title->SetFont( FF_FONT1 , FS_BOLD );
$graph->xaxis->title->SetFont( FF_FONT1 , FS_BOLD );

$p2->SetLegend("FTP_DOWN");

$graph->yaxis->SetTitle('Debit en Mo/s','middle'); 



// Output line
$graph->Stroke();
?>