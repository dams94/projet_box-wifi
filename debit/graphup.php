<?php
require_once ('../jpgraph/src/jpgraph.php');
require_once ('../jpgraph/src/jpgraph_line.php');

$datay1 = array();
$datay2 = array();


$bdd = new PDO('mysql:host=127.0.0.1;dbname=res', 'root', '');
$i=4;
$req=$bdd->prepare("SELECT * FROM `debit` WHERE mode='up' ORDER BY id DESC LIMIT 0, 5 ");
$req->execute();
foreach($req as $value1) {
	$datay1[$i]=$value1['vitesse'];

	$i--;
}
$bdd=null;

/*$bdd1 = new PDO('mysql:host=127.0.0.1;dbname=res', 'root', '');
$i=4;
$req1=$bdd1->prepare("SELECT * FROM `debit` WHERE mode='up' ORDER BY id DESC LIMIT 0, 5 ");
$req1->execute();
foreach($req1 as $value2) {
	$datay2[$i]=$value2['vitesse'];

	$i--;
}
$bdd1=null;*/

$graph = new Graph(960,600);
//$graph->ClearTheme(); 
$graph->title->Set('Historique des 5 derniers tests de debit montant');
$graph->title->SetFont(FF_FONT1,FS_BOLD, 10);
$graph->SetScale('intlin');
$graph->graph_theme = null;
$graph->SetBox(false);

$graph->yaxis->HideZeroLabel();
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

$graph->xaxis->SetTickLabels(array('1','2','3','4','5'));
$graph->ygrid->SetFill(false);

$p1 = new LinePlot($datay1);
//$p2 = new LinePlot($datay2);

$graph->Add($p1);
//$graph->add($p2);

$p1->SetColor('#55bbdd');
$p1->SetLegend('Line 1');
$p1->mark->SetType(MARK_FILLEDCIRCLE,'',1.0);
$p1->mark->SetColor('#55bbdd');
$p1->mark->SetFillColor('#55bbdd');
$p1->value->Show();
$p1->value->SetColor('#55bbdd');
$p1->value->SetFormat('%01.2f');
$p1->SetCenter();


/*$p2->SetColor('red@0.4');
$p2->SetLegend('Line 2');
$p2->mark->Show();
$p2->mark->SetType(MARK_UTRIANGLE,'',1.0);
$p2->mark->SetColor('red@0.4');
$p2->mark->SetFillColor('red@0.4');
$p2->value->SetMargin(14);
$p2->value->Show();
$p2->value->SetColor('red@0.4');
$p2->value->SetFormat('%01.2f');
$p2->SetCenter();*/

$graph->legend->SetMarkAbsSize(8);
$graph->legend->SetFrameWeight(1);
$graph->yaxis->title->SetFont( FF_FONT1 , FS_BOLD );
$graph->xaxis->title->SetFont( FF_FONT1 , FS_BOLD );
$p1->SetLegend("FTP");
//$p2->SetLegend("FTP_DOWN");

$graph->yaxis->SetTitle('Debit en Mo/s','middle'); 



// Output line
$graph->Stroke();


?>